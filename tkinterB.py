from tkinterparent import *

# B IS THE TEACHER
class Application(tkinterparent):
    def callNao(self, text):
        subprocess.call(["python", config.globalPath+"NAOstory-teacher/server/NaoStory.py", text])

    def createWidgets(self):
        self.createText("Voice lines", 1)
        self.createText("Story", 2)
        self.createText("Extra voice lines", 6)

        self.createButton("Bhello")

        self.createText("O say hello", 1, "red")

        self.createButton("Bname", 1, "Ask for name")
        self.createButton("Bmeet", 1, "Nice to meet you")

        self.createText("Go to O", 1, "red")

        self.createButton("fluintro")

        self.createButton("flu1.txt", 2)

        self.createText("Tell story")

        self.createButton("BtransferToGames")

        self.createText("Go to O", 1, "red")

        self.createButton("Bbye", 1, "B says Bye")

        #self.createButtonName("fluStory", "FluEnglish")

root = Tk()
root.title('B Teacher')
app = Application(master=root)
app.mainloop()
root.destroy()

