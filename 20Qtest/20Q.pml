<?xml version="1.0" encoding="UTF-8" ?>
<Package name="special-education-template-standup" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="." xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="icon" src="icon.png" />
        <File name="asknao-manifest" src="asknao-manifest.xml" />
        <File name="translations" src="translations.csv" />
        <File name="choice_sentences" src="Aldebaran/choice_sentences.xml" />
        <File name="applause1" src="sounds/applause1.wav" />
        <File name="popup" src="sounds/popup.wav" />
        <File name="yahou4" src="sounds/yahou4.wav" />
        <File name="20QSource" src="20QSource.c" />
    </Resources>
    <Topics />
    <IgnoredPaths />
</Package>
