from Tkinter import *
import subprocess
import tkinterConfig as config

class tkinterparent(Frame):
    def callNao(self, text):
        pass

    def createWidgets(self):
        pass

    def stopWidget(self):
        self.QUIT = Button(self)
        self.QUIT["text"] = "QUIT"
        self.QUIT["fg"]   = "red"
        self.QUIT["command"] =  self.quit
        self.QUIT.grid(row=7, column=self.rowID[7])
        self.rowID[7] = self.rowID[7] + 1

    def globalWidgets(self):

        self.createButton("think", 6, "Say think")
        self.createButton("hello" , 6, "Say hello")
        self.createButton("wellDone", 6, "Say Well done")

        ## Button we could create:
        #self.createButton("wellDone", 6, "TODO Ramy explain")
        self.createButton("playagain", 6, "TODO play again?")
        #self.createButton("wellDone", 6, "TODO he has to think")
        self.createButton("yes", 6)
        self.createButton("noo", 6)
        self.createButton("Doyouunderstand", 6, "Do you understand?")
        self.createButton("Ready", 6, "Are you ready?")
        self.createButton("wowSoGood.txt", 6, "Wow so good")
        
    def gamesWidgets(self):
        self.createButton("playGames1", 1, "2. Do you want to play?")
        self.createButton("playGames2", 1, "3. Ramy, explain")

        self.createText("PlayGamesHere")

        self.createButton("20Qinstructions", 3, "1.(20Q) We're going to play 20Q")
        self.createButton("20Qinstructions2", 3, "2. (20Q) Ready with animal?")
        self.createButton("1", 3, "3.(20Q)Start game 20Q")
        
        self.createText("", 3)
        self.createButton("3", 3, "1.(RPS) Instructions")
        self.createButton("2", 3, "2.(RPS) Play Game (one time)")

        self.createButton("r", 4, "(RPS) Child did ROCK")
        self.createButton("p", 4, "(RPS) Child did PAPER")
        self.createButton("s", 4, "(RPS) Child did SCISSORS")

        self.createButton("ye", 5, "Answer Yes")
        self.createButton("no", 5, "Answer No")
        self.createButton("mb", 5, "Answer Maybe")
        self.createButton("rp", 5, "Answer repeat Question")

    def text(self, text):
        if len(text) > 2:
    	   self.callNao("storyfiles/" + text)
        else:
           self.callNao(text) 

    def createButton(self, command, rowNumber=1, text=None):
        if  rowNumber > 6 or rowNumber < 0: 
            print("Illegal row" + str(rowNumber))
            rowNumber = 1

        if text == None:
            text = command
        button = Button(self)
        button["text"] = text
        button["command"] = lambda: self.text(command)
        button.grid(row=rowNumber, column=self.rowID[rowNumber])
        button.config(height=2)
        self.rowID[rowNumber] = self.rowID[rowNumber] + 1

    def createText(self, labelText, rowNumber=1, color="yellow"):
        t = Label(self, text=labelText, bg=color)
        t.grid(row=rowNumber, column=self.rowID[rowNumber])
        self.rowID[rowNumber] = self.rowID[rowNumber] + 1

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.rowID = [0, 0, 0, 0, 0, 0, 0, 0]
        self.stopWidget()
        self.createWidgets()
        self.globalWidgets()
        for i in range(1, 15):
            self.columnconfigure(i, minsize=0, pad=1)
        for j in range(1,7):
            self.rowconfigure(j, minsize=50, pad=1)
        self.pack()