# NAOStory
System for creating stories that the NAO or Pepper robot can tell to others using text to speech and animations.

## Usage
* Upload choregraphe program to your robot.
* Edit server/config.py to set your robots ip and port as well as default speech parameters and eye colors

To write a story take a look at examples directory. You have to write at least a story file defining what each character should say and how the robot should behave (moving head, plaing sounds, ...). You can also define a character file that defines different eye colors and and voice speed and pitch for using different charcater representations.

To start NAOStory start the choregraphe program and than start server/NAOStory.py:

     python NaoStory.py <storyFile> [optional: <charFile>]

It's useful, if you upload all files to your robot and use ssh to connect to the robot and start NAOStory from there. Else you need the python NAOqi SDK installed on your comnputer.
