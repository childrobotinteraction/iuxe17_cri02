# ----------------------------------------------------------------------------
# readBook
#
# Autor: Hannes Eilers
# Beschreibung: readBook stellt Funktionen zur Simulation eines Lesebewegung
# des Kopfs des Nao zur Verfuegung. Neben einem kontinuierlichen Lesen, koennen
# auch beliebige Positionen zum aufgucken angegeben werden.
# ----------------------------------------------------------------------------


# ------------------------------------
# IMPORTS MODULES
from naoqi import ALProxy
from time import sleep
# ------------------------------------

# ------------------------------------
# Lesen
def read( naoIP, naoPort ):
	try:
		mem = ALProxy( 'ALMemory', naoIP, naoPort )
		print "read"
		mem.raiseMicroEvent( "naostory_readBook", True )
	except:
		return False
		
	return True
# ------------------------------------


# ------------------------------------
# Lesen stoppen
def stopRead( naoIP, naoPort ):
	try:
		mem = ALProxy( 'ALMemory', naoIP, naoPort )
		mem.raiseMicroEvent( "naostory_readBook", False )
	except:
		return False
		
	return True
# ------------------------------------


# ------------------------------------
# Aufgucken
def lookUp( alpha, naoIP, naoPort ):
	try:
		mem = ALProxy( 'ALMemory', naoIP, naoPort )
	
		stopRead( naoIP, naoPort )
		mem.raiseMicroEvent( "naostory_lookup", alpha )
	except:
		return False
		
	return True
# ------------------------------------
