from tkinterparent import *

class Application(tkinterparent):
    def callNao(self, text):
        subprocess.call(["python", config.globalPath+"NAOstory-master/server/NaoStory.py", text])

    def createWidgets(self):
        self.createText("Voice lines", 1)
        self.createText("Story", 2)
        self.createText("Games", 3)
        self.createText("RPS signs", 4)
        self.createText("Question answers", 5)
        self.createText("Extra voice lines", 6)

        self.createButton("BOhello")

        self.createButton("Bname", 1, "Ask for name")
        self.createButton("Bmeet", 1, "Nice to meet you")

        self.gamesWidgets()

        self.createButton("BOtransferToStory")

        self.createButton("fluintro")

        self.createText("Tell story")

        self.createButton("flu1.txt", 2)

        self.createButton("BOtransferToGames")

        self.createText("Play games")

        self.createButton("BObye")

root = Tk()
root.title('BO')
app = Application(master=root)
app.mainloop()
root.destroy()

