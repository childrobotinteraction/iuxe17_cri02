# ------------------------------------
# CHARS
# ------------------------------------

# ------------------------------------
# IMPORTS
import os
import config
# ------------------------------------

# ------------------------------------
# Read Char-file
def rCharFile( path, charVar ):
	
	# Pruefen ob Datei vorhanden
	if os.path.exists( path ) and os.path.isfile( path ):
		
		# Datei vorhanden
		f = open( path, 'r' )
		
		# Datei zeilenweise einlesen
		for line in f:
		
			# Auf Leerzeile oder Kommentar pruefen
			if line != "\n" and not line.startswith( '#', 0 ):
			
				# Zeile verarbeiten
				line.strip()
				nChar = line.split( ',' )
			
				if len(nChar) > 2:
					nChar[0] = nChar[0].lower()	# Char-Name
					nChar[1] = int( nChar[1] )	# Voiceshape
					nChar[2] = int( nChar[2] )	# Voicespeed
					
					if len(nChar) > 3:
						nChar[3] = int( nChar[3], 16 )
					else:
						nChar.append( int(config.stdEyeColor) )
					charVar.append( nChar )	
		
		
		return True
	
	else:
		return False
# ------------------------------------


# ------------------------------------
# Get Char-Parameter
def getCharParams( char, charVar ):
	
	char = char.lower()
	
	# Liste der Charaktere durchgehen
	for cChar in charVar:
	
		# Ist aktueller Charakter gesuchter?
		if cChar[0] == char:
		
			# Charakter gefunden, zurueckgeben
			return cChar
	
	# Kein Charakter gefunden, Standartparameter laden
	cChar = [ '', config.stdSpeechShape, config.stdSpeechSpeed, int(config.stdEyeColor) ]
	return cChar
	
# ------------------------------------



