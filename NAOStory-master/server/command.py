# ------------------------------------
# COMMANDS
# ------------------------------------

# ------------------------------------
# IMPORTS
from time import sleep
from naoqi import ALProxy
import config
import readBook
# ------------------------------------

# ------------------------------------
# GLOBAL VARS
Sounds = []	# Format: Path, ID
plVolume = 1.0
# ------------------------------------



# ------------------------------------
# Interprete command
def interpCmd( cmd ):
	
	ret = False
	
	if cmd[0] == 'setVol':
		ret = setVol( int(cmd[1]) )
		
	elif cmd[0] == 'setSpeechVol':
		ret = setSpeechVol( int(cmd[1]) )
		
	elif cmd[0] == 'setPlVol':
		ret = setPlVol( int(cmd[1]) )
		
	elif cmd[0] == 'wait':
		ret = wait( int(cmd[1]))
		
	elif cmd[0] == 'plSound':
		ret = plSound( cmd[1], False )
		
	elif cmd[0] == 'plSound_bg':
		ret = plSound( cmd[1], True )
		
	elif cmd[0] == 'stopSound':
		ret = stopSound( cmd[1] )
		
	elif cmd[0] == 'setEyeCol':
		ret = setEyeCol( int(cmd[1], 16) )
		
	elif cmd[0] == 'read':
		readBook.read( config.naoIP, config.naoPort )
		ret = wait(0.5)
	
	elif cmd[0] == 'stopRead':
		readBook.stopRead( config.naoIP, config.naoPort )
		ret = wait(0.5)
	
	elif cmd[0] == 'lookUp':
		readBook.lookUp( int(cmd[1]), config.naoIP, config.naoPort )
		ret = wait(0.5)
		
	elif cmd[0] == 'setLanguage':
		ret = setLanguage( cmd[1].strip() )
		print(cmd[1].strip())
		
	elif cmd[0] == 'setBodytalk':
		ret = setBodytalk( bool( int(cmd[1].strip()) ) )
		
	if not ret:
		print "\nERROR: Fehle beim Ausfuehren des Befehls "+str(cmd)
		
# ------------------------------------


# ------------------------------------
# Play Sound
def plSound( path, bg ):

	# Verbindung mit ALAudioPlayer herstellen
	player = ALProxy( 'ALAudioPlayer', config.naoIP, config.naoPort )
	
	# Path verarbeiten
	path = str(path)
	path = path.strip()
	
	# Laustaerke setzen
	global plVolume
	
	
	print "play soundfile |"+path+"| with volume "+str( int(plVolume * 100) )+"%"
	print "\tbg = " + str(bg) + ""	
	
	
	# Datei abspielen
	if not bg:
        	try: ID = player.post.playFile( path, plVolume, 0.0 )
        	except: return False
        	
        else:
        	try: ID = player.post.playFileInLoop( path, plVolume , 0.0 )
        	except: return False
        
        Sounds.append( [path, ID] )
        
        if not bg:
        	# ggf. auf Ende des Abspielens warten
        	player.wait( ID, 0 )
        
        	# Apsielvorgang wider aus Liste entfernen
        	Sounds.remove( [path, ID] )
        	
        return True
# ------------------------------------


# ------------------------------------
# Stop Sound
def stopSound( path ):
	
	# Path verarbeiten
	path = str(path)
	path = path.strip()
	
	print "stop soundfile "+path+""
	
	# Liste der abspielenden Sound durchgehen
	for sound in Sounds:
		
		# Gesuchter Vorgang?
		if sound[0] == path:
			
			# Verbindung mit ALAudioPlayer herstellen
			player = ALProxy('ALAudioPlayer', config.naoIP, config.naoPort, True )			
			
			# Abspielvorgang stoppen
			try:
				player.stop( sound[1] )
			except:
				return False

			# Apsielvorgang wider aus Liste entfernen
        		Sounds.remove( [sound[0], sound[1]] )
			
			return True
	
	return False
			
# ------------------------------------


# ------------------------------------
# Player volume
def setPlVol( vol ):

	global plVolume
	plVolume = float( vol / 100.0 )
	print "player volume set to " + str(vol) + "%"
	
	return True
# ------------------------------------


# ------------------------------------
# Set master volume
def setVol( vol ):
	
	player = ALProxy( "ALAudioDevice", config.naoIP, config.naoPort )
	print "master volume set to " + str(vol) + "%"
	try: player.setOutputVolume( vol )
	except: return False
	
	return True
# ------------------------------------

# ------------------------------------
# Set speech volume
def setSpeechVol( vol ):
	
	vol = float(vol / 100.0)
	
	tts = ALProxy( "ALTextToSpeech", config.naoIP, config.naoPort )
	print "speech volume set to " + str( int(vol * 100) ) + "%"
	try: tts.setVolume( vol )
	except: return False
	
	return True
# ------------------------------------


# ------------------------------------
# Say a text
def say( text, shape, speed, color, id):

	print("ID is: " + str(id))

	if id == "B":
		ip = config.bIP
		port = config.bPort
		print("B talking")
	elif id == "O":
		ip = config.oIP
		port = config.oPort
		print("O talking")
	else:
		ip = config.naoIP
		port = config.naoPort

	# Verbindung mit ALBroker herstellen
	tts = ALProxy( "ALTextToSpeech", ip, port)
	mem = ALProxy( 'ALMemory', ip, port )	
	
	# Sprachausgabe formatieren
	sentence = "\RSPD="+ str( speed )+ "\ "
        sentence += "\VCT="+ str( shape ) + "\ "
        sentence += text
        sentence +=  "\RST\ "
	
	# AUugenfarbe setzen
	setEyeCol( color )
	
	# Text ausgeben und bis Ende warten
	if config.bodytalk:
		mem.raiseMicroEvent( "naostory_say_bodytalk", sentence )
		while mem.getData( "naostory_talking" ) :
			try: sleep(0.1)
			except: pass
	else:
		ID = tts.post.say( str(sentence) )
		tts.wait( ID, 0 )
	
	return True
# ------------------------------------


# ------------------------------------
# Set eye-color
# !! color need to be integer !!
def setEyeCol( color ):
	
	# Verbindung mit ALBroker herstellen
	led = ALProxy( "ALLeds", config.naoIP, config.naoPort )

	# color in Integer umwandeln
	color = int(color)
	
	# Augenfarbe setzen
	try: led.fadeRGB( "FaceLeds", int(color), float(config.stdEyeColorFade) )
	except: return False
	return True
# ------------------------------------


# ------------------------------------
# Wait
def wait( sec):

	try:
		print "sleep ", sec
		sleep( sec )
	except:
		print "nope wait"
		return False
	
	return True
# ------------------------------------

# ------------------------------------
# Enable/Disable body talk
def setBodytalk( val ):

	try: config.bodytalk = val
	except: return False
	
	return True
# ------------------------------------

# ------------------------------------
# Set language
def setLanguage( lang ):

	try:
		mem = ALProxy( 'ALMemory', config.naoIP, config.naoPort )
		mem.raiseMicroEvent( "naostory_language", lang )
	except: return False
	
	return True
# ------------------------------------

def rockpaperscissors(text):
	try:
		mem = ALProxy( 'ALMemory', config.oIP, config.oPort )
		mem.raiseMicroEvent( "rockpaperscissors", text )
	except: return False
	
	return True


def goToGame(text):
	try:
		mem = ALProxy( 'ALMemory', config.oIP, config.oPort )
		mem.raiseMicroEvent( "goToChooseMode", text )
	except: return False
	
	return True

def	goInstructions( text ):
	try:
		mem = ALProxy( 'ALMemory', config.oIP, config.oPort )
		mem.raiseMicroEvent( "naostory_rpsinstructions", text )
	except: return False
	
	return True

def questionAnswer(text):
	try:
		mem = ALProxy( 'ALMemory', config.naoIP, config.naoPort )
		mem.raiseMicroEvent( "naostory_questionanswer", text )
	except: return False
	return True
