# ------------------------------------
# STORYS and COMMANDS
# ------------------------------------

# ------------------------------------
# IMPORTS
import os
import chars
import config
import command
# ------------------------------------


# ------------------------------------
# Interprete line
def interprLine( line, cVar ):
	
	# Zeile verarbeiten
	line.strip()
	
	if line.startswith( '<', 0 ):
	
		# Zeile ist Befehl
		
		# Befehl und Parameter aufteilen
		cmd = line.split( '>' )
		
		# Befehl verarbeiten
		cmd[0] = cmd[0].replace( '<', '' )
		
		# Befehl interpretieren			
		command.interpCmd( cmd )
		
	else:
	
		# Zeile ist Text
		
		# Charakter erkennen
		line.strip()
		line = line.split( ':' )
		
		# Ueberpruefen, ob Charakter angegeben
		if len(line) == 2:
			
			# Charakterparameter holen
			char = chars.getCharParams( line[0], cVar )
			
			# Text ausgeben
			command.say( line[1], char[1], char[2], char[3], line[0])
					
		else:
			return False
# ------------------------------------


# ------------------------------------
# Read Story-file
def rStoryFile( path, cVar ):
	
	# Pruefen ob Datei existiert
	if os.path.exists( path ) and os.path.isfile( path ):
		
		# Datei oeffnen
		f = open( path, 'r' )
		
		i = 1
		
		# Datei zeilenweise auslesen
		for line in f:
			
			# Auf Leerzeile oder Kommentar pruefen
			if line != "\n" and not line.startswith( '#', 0 ):
			
				# Zeile intepretieren
				ret = interprLine( line, cVar)
			
				# Zeile auswertbar?
				if ret == False:
					print( "\nLine "+str(i)+": Fehler beim Auswerten" )
			
				# Zaehler inkrementieren
				i = i + 1
			
		
		return True
	else:
		print( "\nERROR: story-file "+str(path)+" doesn't exsists or is not a readable file" )
		return False
# ------------------------------------
