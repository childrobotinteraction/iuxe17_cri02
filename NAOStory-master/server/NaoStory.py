# ----------------------------------------------------------------------------
# ---- NaoStory ----
# Version 0.1
#
# Autor:         Hannes Eilers
# Mail:          mail@private-factory.de
#
# License:       Creative Commons by-nc-sa Version 3.0 (CC BY-NC-SA 3.0)
#                http://creativecommons.org/licenses/by-nc-sa/3.0/
#
# Dokumentation: siehe doc/ Unterordner
#
# Beschreibung:
# NaoStory laesst den Nao eine Geschichte aus einer Textdatei vorlesen
# Zusaetzliche Sprachparameter fuer einzelne Charaktere koennen ueber eine weitere
# Textdatei geladen werden.
#
# SYNTAX:
# NaoStory.py <storyFile> [optional: <charFile>]
# ----------------------------------------------------------------------------

# ------------------------------------
# IMPORTS MODULES
from sys import *
# ------------------------------------

# ------------------------------------
# IMPORTS EXT FILES
import chars
import story
import config
import command
import readBook
# ------------------------------------

# ------------------------------------
# GLOBAL VARS
Chars = []
# ------------------------------------


# ------------------------------------
# MAIN

# Anzahl der Uebergaeparameter auswerten
if len(argv) == 1:

        # Nicht genuegend Argumente
        print "\nERROR: Not enough arguments\n"

elif len(argv) == 2:

        if len(argv[1]) < 3:
                if argv[1] == "r":
                         command.rockpaperscissors( "r" )
                         
                if argv[1] == "p":
                        command.rockpaperscissors( "p" )
                        
                if argv[1] == "s":
                        command.rockpaperscissors( "s" )

                if argv[1] == "1":
                        command.goToGame( "Mode1: game" )
                if argv[1] == "2":
                        command.goToGame( "Mode2: game" )
                if argv[1] == "3":
                        command.goInstructions( "3" )

                if argv[1] == "ye": # yes
                        command.questionAnswer( "0" )
                if argv[1] == "no": # no
                        command.questionAnswer( "1" )
                if argv[1] == "mb": # Maybe
                        command.questionAnswer( "2" )
                if argv[1] == "rp": #Repeat
                        command.questionAnswer( "3" )
        else:        
                # Nur Story-File

                # Lautstaerke Sprache setzen
                command.setSpeechVol( config.stdSpeechVolume )

                # Lautstaerke Player setzen
                command.setPlVol( config.stdPlayerVolume )

                # Laustaerke Master setzen
                command.setVol( config.stdMasterVolume )

                # Story-File auslesen
                story.rStoryFile( argv[1], Chars )


elif len(argv) > 2:

        # Story-File und Char-File

        # Lautstaerke Sprache setzen
        command.setSpeechVol( config.stdSpeechVolume )

        # Lautstaerke Player setzen
        command.setPlVol( config.stdPlayerVolume )

        # Laustaerke Master setzen
        command.setVol( config.stdMasterVolume )

        # Char-File auslesen
        chars.rCharFile( argv[2], Chars )

        # Story-File auslesen
        story.rStoryFile( argv[1], Chars )

# ------------------------------------
