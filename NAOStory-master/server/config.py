# ------------------------------------
# CONFIG
naoIP = "192.168.0.101"
naoPort = 9559			# Standart is 9559

oIP = naoIP
oPort = naoPort

bIP = "192.168.0.101"
bPort = 9559


stdSpeechShape = 100
stdSpeechSpeed = 90
stdSpeechVolume = 60	# 0 - 100 %
stdSpeechVoice = 0

stdPlayerVolume = 30	# 0 - 100 %
stdMasterVolume = 70	# 0 - 100 %

stdEyeColor = 0x0000FF	# RGB Eye-Color
stdEyeColorFade = 0		# Time to fade the eye-color in seconds
bodytalk = False
# ------------------------------------

